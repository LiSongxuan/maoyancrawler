import re
from urllib.request import urlretrieve, urlopen, Request
from fontTools.ttLib import TTFont
def compare(f1,f2):
    pp1=[]
    pp2=[]
    index1=0
    index2=0
    for f in f1:
        pp1.append(f)
    for f in f2:
        pp2.append(f)
    flag = 0
    for i in range(index1,len(pp1)):
        for j in range(index2,min(index2+2,len(pp2))):
            if abs(pp1[i][0]-pp2[j][0])<40 and abs(pp1[i][1]-pp2[j][1])<40 :
                flag=flag+1
                index1=i+1
                index2=j+1
    if len(pp1) - flag > len(pp1) * 0.3:
        return False
    return True

def process_font(url):
    # loc.woff是事先下载好的字体文件
    # 可以通过font1.saveXML()来了解文件的结构, font1就像一个的字典, XML文件里的tag可以对font1用字典的方法获取
    font1 = TTFont('loc.woff')
    # 使用百度的FontEditor手动确认本地字体文件name和数字之间的对应关系, 保存到字典中
    loc_dict = {'uniE0A5': '5', 'uniF8EC': '3', 'uniEAED': '8', 'uniF06D': '1', 'uniE00C': '2', 'uniE41A': '4',
                'uniF4F3': '9', 'uniE5D6': '6', 'uniF4D3': '7', 'uniE870': '0'}
    # 获取字符的name列表, 打印出来后发现第一个和最后一个name所对应的不是数字, 所以切片
    uni_list1 = font1.getGlyphNames()[1: -1]

    # 网页源码
    rsp = urlopen(Request(url, headers={'User-Agent': 'Mozilla'})).read().decode()
    # 获取动态的字体文件并下载
    font_url = 'http://' + re.findall(r'url\(\'//(.*?\.woff)', rsp)[0]
    # web字体文件落地名
    filename = font_url.split('/')[-1]
    # 下载web字体文件
    urlretrieve(font_url, filename)
    # 打开web字体文件
    font2 = TTFont(filename)
    # 获取字符的name列表
    uni_list2 = font2.getGlyphNames()[1: -1]
    # web字体文件中name和num映射
    new_map = {}
    i=0
    for uni2 in uni_list2:
        # 获取name 'uni2' 在font2中对应的对象
        obj2 = font2['glyf'][uni2].coordinates
        for uni1 in uni_list1:
            # 获取name 'uni1' 在font1中对应的对象
            obj1 = font1['glyf'][uni1].coordinates
            # 如果两个对象相等, 说明对应的数字一样
            if compare(obj1,obj2):
                # 将name键num值对加入new_map
                new_map[uni2] = loc_dict[uni1]
    for uni1 in uni_list1:
        print(uni1)
        print(font1['glyf'][uni1].coordinates)
    print(new_map)
    # 将数字替换至源码
    for i in uni_list2:
        print(i)
        pattern = '&#x' + i[3:].lower() + ';'
        rsp = re.sub(pattern, new_map[i], rsp)

    # 返回处理处理后的源码
    return rsp


if __name__ == '__main__':
    # 猫眼国内实时票房top10
    url = 'https://maoyan.com/board/1'
    # 替换数字后的网页源码
    res = process_font(url)
    print(res)
